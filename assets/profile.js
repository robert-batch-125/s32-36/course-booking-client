
let token = localStorage.getItem('token')

let name = document.querySelector('#userName');
let email = document.querySelector('#email');
let mobileNo = document.querySelector('#mobileNo')
let editButton = document.querySelector('#editButton')
let enrollContainer = document.querySelector('#enrollContainer')

fetch('http://localhost:3000/api/users/details',
	{
		method: "GET",
		headers: {
			"Authorization": `Bearer ${token}`
		}
	}
)
.then(result => result.json())
.then( result => {
	console.log(result)

		//to display user details in the web page
		name.innerHTML = `${result.firstName} ${result.lastName}`
		email.innerHTML = result.email
		mobileNo.innerHTML = result.mobileNo
		editButton.innerHTML =
		`
			<div class="mb-2">
				<a href="./editProfile.html" class="btn btn-primary">Edit Profile</a>
			</div>
		`

	console.log(result)

	result.enrollments.forEach( course => {

		let courseId = course.courseId

		fetch(`http://localhost:3000/api/courses/${courseId}`, {
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			console.log(course)
			console.log(result)

			let card = 
			`
				<div class="card">
					<div class="card-body">
						<h5 class="card-title">${result.name}</h5>
						<p class="card-text">${result.description}</p>
						<p class="card-text">${course.enrolledOn}</p>
					</div>
				</div>
			`

			enrollContainer.insertAdjacentHTML("beforeend", card);
		})
	})
})